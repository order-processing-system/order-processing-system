package com.order.service;

import com.order.entity.Order;
import com.order.entity.Product;

import java.util.List;
import java.util.Map;

public interface ProductService {

    Map<Long, Product> addProduct(Product product);

    Map<Long, Product> removeProduct(Long productId);

    List<Product> fetchAllProducts();

    List<Order> fetchAllOrders();

    Order placeOrder(Product product);

    Map<Long, Order> cancelOrder(Long orderId);

}
