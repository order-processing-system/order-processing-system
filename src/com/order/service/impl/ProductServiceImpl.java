package com.order.service.impl;

import com.order.entity.Order;
import com.order.entity.Product;
import com.order.service.ProductService;

import java.util.*;

public class ProductServiceImpl implements ProductService {


    @Override
    public Map<Long, Product> addProduct(Product product) {
        Map<Long, Product> productMap = new HashMap<>();
        productMap.put(product.getProductId(), product);
        return productMap;
    }

    @Override
    public Map<Long, Product> removeProduct(Long productId) {
        Map<Long, Product> productMap = new HashMap<>();
        Product product = new Product(1L, "Pixel");
        productMap.put(1L, product);
        productMap.remove(productId);
        return  productMap;
    }

    @Override
    public List<Product> fetchAllProducts() {
        List<Product> productList = new ArrayList<>();
        productList.add(new Product(1L, "Pixel"));
        productList.add(new Product(1L, "Laptop"));
        return productList;
    }

    @Override
    public List<Order> fetchAllOrders() {
        List<Order> orderList = new ArrayList<>();
        Product product = new Product(1L, "Pixel");
        orderList.add(new Order(1L, product));
        return orderList;
    }

    @Override
    public Order placeOrder(Product product) {
        Order order = new Order(1L, product);
        return order;
    }

    @Override
    public Map<Long, Order> cancelOrder(Long orderId) {
        Map<Long, Order> orderMap = new HashMap<>();
        Product product = new Product(1L, "Pixel");
        Order order = new Order(1L, product);
        orderMap.put(order.getOrderId(), order);
        orderMap.remove(orderId);
        return  orderMap;
    }
}
